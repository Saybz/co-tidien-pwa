export const getCategories = (items = []) => {
  return items.reduce((accumulator, item) => {
    return {
      ...accumulator,
      [item.category]: [...(accumulator[item.category] ?? []), item],
    };
  }, {});
};
