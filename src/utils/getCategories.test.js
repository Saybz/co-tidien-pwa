import { getCategories } from "./getCategories";

test("basic", () => {
  expect(
    getCategories([
      { name: "banane", category: "fruit" },
      { name: "carotte", category: "légume" },
      { name: "pomme", category: "fruit" },
    ])
  ).toEqual({
    fruit: [
      { name: "banane", category: "fruit" },
      { name: "pomme", category: "fruit" },
    ],
    légume: [{ name: "carotte", category: "légume" }],
  });
});
